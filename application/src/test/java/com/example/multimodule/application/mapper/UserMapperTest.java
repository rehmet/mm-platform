package com.example.multimodule.application.mapper;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.example.multimodule.application.model.User;
import com.example.multimodule.application.model.enums.Sex;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;
import java.util.Map;

/**
 * @author rt
 * Copyright (c) 2022 by rt
 * All rights reserved.
 */
@SpringBootTest
public class UserMapperTest {

    @Autowired
    private UserMapper userMapper;

    @Test
    public void testSelect() {
        System.out.println(("----- selectAll method test ------"));
        List<User> userList = userMapper.selectList(null);
        userList.forEach(System.out::println);
    }

    @Test
    public void testAdd() {
        User user = new User();
        user.setName("Richart");
        //user.setId(23L);
        userMapper.insert(user);
        QueryWrapper<User> queryWrapper = new QueryWrapper<>();
        queryWrapper.orderBy(true, true, "id");
        List<User> users = userMapper.selectList(queryWrapper);
        users.forEach(System.out::println);
    }

    @Test
    public void testSelect1() {
        QueryWrapper<User> queryWrapper = new QueryWrapper<>();
        queryWrapper.lambda().eq(User::getId, 1);
        List<User> users = userMapper.selectList(queryWrapper);
        users.forEach(System.out::println);
    }

    @Test
    public void testCustom() {
        Map<String, Object> map = userMapper.selectMapById(1573789750000533507L);
        System.out.println("map = " + map);
    }

    @Test
    public void testLamda() {
        LambdaQueryWrapper<User> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.isNotNull(User::getEmail);
        List<User> users = userMapper.selectList(queryWrapper);
        users.forEach(System.out::println);
    }

    @Test
    public void testAddEnum() {
        User user = new User();
        user.setSex(Sex.MALE);
        user.setName("Jackson");
        user.setAge(23);

        userMapper.insert(user);
    }

    @Test
    public void optimistic() {
        LambdaUpdateWrapper<User> userLambdaUpdateWrapper = new LambdaUpdateWrapper<>();
        userLambdaUpdateWrapper.eq(User::getSex, 1);
        User user = new User();
        user.setEmail("man@microsoft.com");

        userMapper.update(user, userLambdaUpdateWrapper);

    }

    @Test
    public void testDesc() {

        QueryWrapper<User> qw = new QueryWrapper<>();
        qw.lambda().orderBy(true, true, User::getId);
        List<User> users = userMapper.selectList(qw);

        users.forEach(System.out::println);
    }
}
