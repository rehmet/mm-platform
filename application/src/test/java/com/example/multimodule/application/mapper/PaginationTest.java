package com.example.multimodule.application.mapper;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.example.multimodule.application.model.User;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

/**
 * @author rt
 * Copyright (c) 2022 by rt
 * All rights reserved.
 */
@SpringBootTest
public class PaginationTest {

    @Autowired
    UserMapper userMapper;

    @Test
    public void testPagination() {

        Page<User> page = new Page<>(1, 3);
        QueryWrapper<User> queryWrapper = new QueryWrapper<>();

        IPage<User> userIPage = userMapper.selectPage(page, null);
        System.out.println("page.getRecords() = " + page.getRecords());
        System.out.println("userIPage.getRecords() = " + userIPage.getRecords());

    }
}
