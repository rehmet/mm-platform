package com.example.multimodule.application.mapper;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.example.multimodule.application.model.User;
import com.example.multimodule.application.service.UserService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;

/**
 * @author rt
 * Copyright (c) 2022 by rt
 * All rights reserved.
 */
@SpringBootTest
public class UserServiceTest {

    @Autowired
    UserService userService;

    @Autowired
    UserMapper userMapper;

    @Test
    public void testCount() {
        long count = userService.count();
        System.out.println("count = " + count);
    }

    @Test
    public void testBatchAdd() {
        Collection<User> list = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            User user = new User();
            user.setName("R" + i);
            list.add(user);
        }
        userService.saveBatch(list);
    }

    @Test
    public void testBatchDel() {
        QueryWrapper<User> qwery = new QueryWrapper<>();
        //userMapper.delete(qwery.ne("name", "WWW"));
        boolean remove = userService.remove(qwery.ne("name", "W"));
        System.out.println("remove = " + remove);
    }

    @Test
    public void testS() {
        Map<String, Object> map = userService.selectMap(1573789750000533507L);
        map.put("AAR", "NEW");
        System.out.println("map = " + map);
    }

    @Test
    public void testupDate() {
        QueryWrapper<User> queryWrapper = new QueryWrapper<>();
        //boolean email = userService.remove(queryWrapper.isNull("email"));
        queryWrapper.ge("age", 21).like("name", "R")
                .or()
                .isNull("email");

        User user = new User();
        user.setName("REHMET");
        user.setEmail("Rehmet@selte.net");

        int result = userMapper.update(user, queryWrapper);

        System.out.println("result = " + result);
    }
}
