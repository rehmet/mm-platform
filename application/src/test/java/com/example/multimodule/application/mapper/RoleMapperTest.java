package com.example.multimodule.application.mapper;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.example.multimodule.application.model.Role;
import com.example.multimodule.application.model.User;
import com.example.multimodule.application.service.UserService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;
import java.util.Map;

/**
 * @author rt
 * Copyright (c) 2022 by rt
 * All rights reserved.
 */
@SpringBootTest
public class RoleMapperTest {

    @Autowired
    RoleMapper roleMapper;

    @Autowired
    UserService userService;

    @Test
    public void testAdd() {
        Role role = new Role();
        role.setRoleName("SYSTEM");
        int insert = roleMapper.insert(role);
        System.out.println("insert = " + insert);
    }

    @Test
    public void testSelect() {
        roleMapper.selectList(null).forEach(System.out::println);
    }

    @Test
    public void testMulti() {

        List<Map<String, Object>> map = userService.selectUserAndRoleName();
        System.out.println("map = " + map);

    }

    @Test
    public void testRoleID() {
        QueryWrapper<User> wrapper = new QueryWrapper<>();
        List<User> users = userService.selectByRoleId("1573965606983462913");
        users.forEach(System.out::println);
    }
}
