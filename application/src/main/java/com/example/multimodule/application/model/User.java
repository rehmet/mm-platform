package com.example.multimodule.application.model;

import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableName;
import com.example.multimodule.application.model.enums.Sex;
import lombok.Data;

/**
 * @author rt
 * Copyright (c) 2022 by rt
 * All rights reserved.
 */
@Data
@TableName("user")
public class User {
    private Long id;
    private String name;
    private Sex sex;
    private Integer age;
    private String email;
    private Role role;
    @TableLogic
    private Integer isDeleted;
}
