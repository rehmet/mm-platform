package com.example.multimodule.application.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.example.multimodule.application.model.Role;

/**
 * @author rt
 * Copyright (c) 2022 by rt
 * All rights reserved.
 */
public interface RoleService
        extends IService<Role> {
}
