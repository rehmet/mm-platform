package com.example.multimodule.application.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.multimodule.application.mapper.UserMapper;
import com.example.multimodule.application.model.User;
import com.example.multimodule.application.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * @author rt
 * Copyright (c) 2022 by rt
 * All rights reserved.
 */
@Service
public class UserServiceImpl
        extends ServiceImpl<UserMapper, User>
        implements UserService {

    @Autowired
    UserMapper userMapper;

    @Override
    public Map<String, Object> selectMap(Long id) {
        return baseMapper.selectMapById(id);
    }

    @Override

    public List<Map<String, Object>> selectUserAndRoleName() {
        return userMapper.selectOnlyNameAndRole();
    }

    @Override
    public List<User> selectByRoleId(String roleId) {
        return userMapper.selectByRoleId(roleId);
    }
}
