package com.example.multimodule.application.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.example.multimodule.application.model.User;

import java.util.List;
import java.util.Map;

/**
 * @author rt
 * Copyright (c) 2022 by rt
 * All rights reserved.
 */
public interface UserService
        extends IService<User> {
    Map<String, Object> selectMap(Long id);

    List<Map<String, Object>> selectUserAndRoleName();

    List<User> selectByRoleId(String roleId);
}
