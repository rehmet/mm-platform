package com.example.multimodule.application.model.enums;

import com.baomidou.mybatisplus.annotation.EnumValue;
import lombok.Getter;

/**
 * @author rt
 * Copyright (c) 2022 by rt
 * All rights reserved.
 */
@Getter
public enum Sex {
    MALE(1, "男"),
    FEMALE(1, "女");

    @EnumValue
    private int sex;
    private String sexname;

    Sex(int sex, String sexname) {
        this.sex = sex;
        this.sexname = sexname;
    }
}
