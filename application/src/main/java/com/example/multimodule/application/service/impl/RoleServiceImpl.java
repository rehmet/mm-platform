package com.example.multimodule.application.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.multimodule.application.mapper.RoleMapper;
import com.example.multimodule.application.model.Role;
import com.example.multimodule.application.service.RoleService;
import org.springframework.stereotype.Service;

/**
 * @author rt
 * Copyright (c) 2022 by rt
 * All rights reserved.
 */
@Service
public class RoleServiceImpl
        extends ServiceImpl<RoleMapper, Role>
        implements RoleService {
}
