package com.example.multimodule.application.model.vo;

import com.example.multimodule.application.model.Role;
import com.example.multimodule.application.model.User;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author rt
 * Copyright (c) 2022 by rt
 * All rights reserved.
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class UserWithRoleVO extends User {
    private Role role;
}
