package com.example.multimodule.application.model;

import lombok.Data;

/**
 * @author rt
 * Copyright (c) 2022 by rt
 * All rights reserved.
 */
@Data
public class Role {
    private Long id;
    private String roleName;
}
