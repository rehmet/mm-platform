package com.example.multimodule.application.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.multimodule.application.model.User;
import org.apache.ibatis.annotations.MapKey;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Repository
public interface UserMapper
        extends BaseMapper<User> {

    @MapKey("selectMapById")
    Map<String, Object> selectMapById(@Param("id") Long id);

    @Select("select u.id, u.name, r.role_name from user u, role r where u.role_id=r.id")
    List<Map<String, Object>> selectOnlyNameAndRole();

    @MapKey("selectByRoleId")
    List<User> selectByRoleId(@Param("roleId") String roleId);
}
