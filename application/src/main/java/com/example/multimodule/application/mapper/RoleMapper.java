package com.example.multimodule.application.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.multimodule.application.model.Role;
import org.springframework.stereotype.Repository;

/**
 * @author rt
 * Copyright (c) 2022 by rt
 * All rights reserved.
 */
@Repository
public interface RoleMapper
        extends BaseMapper<Role> {
}
