package com.example.multimodule.mapper;

import com.example.multimodule.model.Blog;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

/**
 * @author rt
 * Copyright (c) 2022 by rt
 * All rights reserved.
 */
@SpringBootTest
class BlogMapperTest {

    @Autowired
    private BlogMapper blogMapper;

    @Test
    public void testSelect() {
        List<Blog> blogList = blogMapper.selectList(null);
        blogList.forEach(System.out::println);
    }

    @Test
    public void add() {
        Blog blog = new Blog();
        blog.setTitle("Normand New");
        blogMapper.insert(blog);
        List<Blog> blogs = blogMapper.selectList(null);
        blogs.forEach(System.out::println);
    }
}
