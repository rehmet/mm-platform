package com.example.multimodule.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.multimodule.model.Blog;
import org.springframework.stereotype.Repository;

/**
 * @author rt
 * Copyright (c) 2022 by rt
 * All rights reserved.
 */
@Repository
public interface BlogMapper extends BaseMapper<Blog> {
}
