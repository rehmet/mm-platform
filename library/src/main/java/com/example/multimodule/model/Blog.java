package com.example.multimodule.model;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

/**
 * @author rt
 * Copyright (c) 2022 by rt
 * All rights reserved.
 */
@Data
@TableName("demo2_blog")
public class Blog {
    Integer id;
    String title;
}
